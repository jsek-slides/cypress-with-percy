# Example integration with Percy for Cypress

## Setup

```shell
npm i
```

### Run on localhost

```shell
npm run watch
```

and in another terminal tab

```shell
npm run serve
```