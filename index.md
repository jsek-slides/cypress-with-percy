---
marp: true
title: Cypress with Percy
description: Cypress - Example integration with Percy
theme: uncover
paginate: true
_paginate: false
---

<style>
@import url(https://fonts.googleapis.com/css?family=Montserrat|PT+Mono);
section {
  font-family: 'Montserrat', 'Segoe UI', Ubuntu, sans-serif;
  letter-spacing: 1px;
  background: #f2f2f4;
  color: #222;
}
img[alt='logo'] {
  background: #fff;
  padding: 1em 20%;
}
a {
  color: #00bcd4;
}
section code {
  font-family: 'PT Mono', Consolas, 'Ubuntu Mono', monospace;
}
li > code,
p > code {
  font-size: .9em;
  background-color: transparent;
  color: #ff9800;
}
pre {
  font-family: 'PT Mono', Consolas, 'Ubuntu Mono', monospace;
  padding: 1.4em 2em;
  box-shadow: none;
  font-size: 65%;
  line-height: 1.35;
}

.hljs-comment {
  color: #595 !important;
  font-style: italic;
  font-size: .8em;
}
</style>

![logo](images/google/logo.png)

How to integrate and use it with Cypress

---

### Nice things about Percy

- Super easy to use
- Auto-approve on specific branches (e.g. `master`)
- Full page screenshots
- Nice dashboard

---

```js
describe('when page is rendered', () => {

  it('should display all sections', () => {

    cy.get('.section-1').should('be.visible')
    cy.get('.section-2').should('be.visible')

    cy.percySnapshot() // <- We want this to work
  })
})
```

---

Install new dependency

```sh
npm install @percy/cypress
```

Reference in commands.js

```js
import '@percy/cypress'
```

---

docker-compose.yml

```yml
version: "3.2"

services:
  website:
    build: .
    container_name: nina-website
    ports:
      - "80:80"

  e2e:
  # here goes interesting stuff
```

---

```yml
e2e:
  build:
    context: .
    dockerfile: cypress/Dockerfile
  container_name: my-cypress
  depends_on:
    - website
  environment:
    - CYPRESS_baseUrl=http://website
    - CYPRESS_RECORD_KEY=XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX
    - PERCY_TOKEN=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    - PERCY_BRANCH=master
  volumes:
    - ./.git:/e2e/.git
    - ./cypress:/e2e/cypress
    - ./cypress.json:/e2e/cypress.json
  entrypoint: npx percy exec -- cypress run -b chrome --record
```

---

cypress/Dockerfile

```dockerfile
FROM cypress/included:3.3.1
WORKDIR /e2e

# Install Google Chrome
RUN blablablabla \
  apt-get install -y google-chrome-stable

# Install Percy
RUN npm init -y
RUN npm i @percy/cypress
```

---

package.json

```json

...
"cypress:up": "docker-compose up 
    --abort-on-container-exit 
    --exit-code-from e2e"
...

```

---

# `> npm run cypress:up`

---

![](images/screenshots/dashboard.png)

---

![](images/screenshots/diff.png)